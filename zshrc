# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="sorohan"
# robbyrussell"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git tmux zsh-syntax-highlighting)

source $ZSH/oh-my-zsh.sh

# User configuration

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/dsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.7.0_65.jdk/Contents/Home
export EDITOR='vim'
export MAVEN_OPTS="-Xmx512m -Xmx1024m -XX:MaxPermSize=1024m"
alias confcp='cp /Users/ben/backbase/launchpad/launchpad-trunk/launchpad-webapps/portalserver/src/main/resources/conf.xml \
    /Users/ben/backbase/launchpad/launchpad-trunk/launchpad-jetty/work/jetty-0.0.0.0-7777-portalserver.war-_portalserver-any-/webapp/WEB-INF/classes/conf.xml'
alias gs='git status'
alias gcm='git co master'
alias gsr='git svn rebase'
alias md5sum='md5'

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8

# backbase
export BACKBASE_PATH=$HOME/backbase
export LAUNCHPAD_PATH=$BACKBASE_PATH/launchpad
export LAUNCHPAD_DEV_PATH=$LAUNCHPAD_PATH/git
export LAUNCHPAD_TRUNK_PATH=$LAUNCHPAD_DEV_PATH/trunk
export LP_PATH=$LAUNCHPAD_PATH
export LP_TRUNK=$LAUNCHPAD_TRUNK_PATH
export LP_DEV=$LAUNCHPAD_DEV_PATH
alias bb-lp="cd $LAUNCHPAD_DEV_PATH"
alias bb-lp-jetty="cd $LAUNCHPAD_TRUNK_PATH/launchpad-jetty"
alias bb-jq="xml2json --strip_text | jq"
alias bb-run="${LAUNCHPAD_TRUNK_PATH}/run.sh"
alias pjetty="pgrep -f jetty"
alias kjetty="pkill -f jetty"
alias bb-link='for c in base core ui theme; do if [ -d "bower_components/$c" ]; then bower link $c; fi; done'
alias bb-mod-link='(cd bower_components; for m in $(ls -d module-*); do (cd .. && bower link $m); done)'
alias bb-status='bb_do git status'
alias bb-pull='bb_do git pull --ff-only'
alias bb=backbase
alias bb-register='bb_register'

function bb_register() {
    name=$(grep '"name"' bower.json 2>/dev/null | awk -F \" '{print $4}');
    url=$(git remote -v 2>/dev/null | grep stash.backbase.com | awk '{print $2}' | head -1);
    bower_registry="http://launchpad.backbase.com:5678/registerPackage"
    if [ ! -z "$name" ] && [ ! -z "$url" ]; then
        echo "registering $name as $url"
        curl --request POST "$bower_registry" -d name='$name' -d repo='$url'
    else
        if [ -z "$name" ]; then
            echo "couldn't find package name in bower.json"
        fi
        if [ -z "$url" ]; then
            echo "couldn't find git-stash URL"
        fi
    fi
}

function bb_do() {
    for d in $(ll bower_components | grep "\->" | awk "{print \$10}"); do
        (echo $fg[green] "\n#$d> $@" $reset_color && \
            cd bower_components/$d && \
            $@\
        );
    done
}

# path
export PATH="/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:$HOME/bin:$LP_TRUNK/bin"
# export MANPATH="/usr/local/man:$MANPATH"
#eval $(ssh-agent)
#ssh-add ~/.ssh/id_rsa
